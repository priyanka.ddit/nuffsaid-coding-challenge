from pathlib import Path
import csv
from collections import defaultdict
data_dir = str(Path().absolute())+'/data'

def first(result1):
    print("\nHow many total schools are in this data set?\n")
    print(len(result1))


def second(result2):
    print("\nHow many schools are in each state?\n")
    for k2, v2 in result2.items():
        print(k2, len(list(filter(None, v2))))


def third(result3):
    print("\nHow many schools are in each Metro-centric locale?\n")
    for k3, v3 in result3.items():
        print(k3, len(list(filter(None, v3))))


def common(result4):
    temp = defaultdict(list)
    for k4, v4 in result4.items():
        temp[k4].append(len(list(filter(None, v4))))
    return temp


def fourth(result4):
    res = common(result4)
    print("\nWhat city has the most schools in it? \n")
    city = max(res, key=res.get)
    print(city)
    print("\nHow many schools does it have in it?\n")
    print(res[city])


def fifth(result4):
    res = common(result4)
    print("\nHow many unique cities have at least one school in it?\n")
    print(res)


def preprocess(csvreader):
    result1 = defaultdict(list)
    result2 = defaultdict(list)
    result3 = defaultdict(list)
    result4 = defaultdict(list)

    for row in csvreader:
        result1[row[0]]
        result2[row[5]].append(row[3])
        result3[row[8]].append(row[3])
        result4[row[4]].append(row[0])

    first(result1)
    second(result2)
    third(result3)
    fourth(result4)
    fifth(result4)


with open(data_dir + '/school_data.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    next(csvreader, None)
    preprocess(csvreader)







