import csv
from pathlib import Path
import re


def search_schools(search_str,csv_file):
    list_items = []
    for row in csv_file:
        if re.findall(search_str,row[3]) or re.findall(search_str,row[4]) or re.findall(search_str,row[5]):
            list_items.append(row[3]+','+row[5])
            if len(list_items) == 3:
                break
    return list_items


data_dir = str(Path().absolute())+'/data'
csv_file = csv.reader(open(data_dir+'/school_data.csv', "r"), delimiter=",")
pattern = input('Enter Name to find\n')
search_str = re.compile(pattern,re.IGNORECASE)
result = search_schools(search_str,csv_file)
print(result)
